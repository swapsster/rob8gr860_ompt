# ROB8 gr860 OMTP Course

## Introduction
During the "Object Manipulation Task Planning" course some lectures had an assignment. These assignments are solved and saved in this repository.

**Repository structure**: Each assignment builds on top of the previous work done. This means that files gets deleted and updated, so to see each hand-in, use the tags.

**Tags**: There is a tag for each lecture with an assignment. Click on the tag to see the status of the project at that time.

- [Lecture 1](https://bitbucket.org/swapsster/rob8gr860_ompt/src/lec1_submission/)
- [Lecture 5](https://bitbucket.org/swapsster/rob8gr860_ompt/src/lec5_submission/)
- [Lecture 6](https://bitbucket.org/swapsster/rob8gr860_ompt/src/lec6_submission/)
- [Lecture 7](https://bitbucket.org/swapsster/rob8gr860_ompt/src/lec7_submission/)
- [Lecture 8](https://bitbucket.org/swapsster/rob8gr860_ompt/src/lec8_submission/)
- [Lecture 9](https://bitbucket.org/swapsster/rob8gr860_ompt/src/lec9_submission/)

Lecture 2 was learning about Git and version control.

Lecture 3 and 4 was guest lectures.

**Git commit style**: This repository tries to follow the [Udacity Git Commit Message Style](https://udacity.github.io/git-styleguide/).

**Getting Started**: Ubuntu 18.04 and ROS Melodic is required. Prerequisites for running Lecture is listed in the beginning of each README.

**Authors**: Lukas Wyon and Søren Myhre Voss

![frontpage-picture](frontpage.png)
