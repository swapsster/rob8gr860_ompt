#!/usr/bin/env python
# -*- coding: utf-8 -*-
###########################################################
#               WARNING: Generated code!                  #
#              **************************                 #
# Manual changes may get lost if file is generated again. #
# Only code inside the [MANUAL] tags will be kept.        #
###########################################################

from flexbe_core import Behavior, Autonomy, OperatableStateMachine, ConcurrencyContainer, PriorityContainer, Logger
from omtp_factory_flexbe_states.srdf_state_to_moveit import SrdfStateToMoveit as omtp_factory_flexbe_states__SrdfStateToMoveit
from omtp_factory_flexbe_states.set_conveyor_power_state import SetConveyorPowerState
from omtp_factory_flexbe_states.control_feeder_state import ControlFeederState
from omtp_factory_flexbe_states.detect_part_camera_state import DetectPartCameraState
from omtp_factory_flexbe_states.compute_grasp_state import ComputeGraspState
from flexbe_manipulation_states.moveit_to_joints_dyn_state import MoveitToJointsDynState as flexbe_manipulation_states__MoveitToJointsDynState
from omtp_factory_flexbe_states.vacuum_gripper_control_state import VacuumGripperControlState
from flexbe_states.wait_state import WaitState
# Additional imports can be added inside the following tags
# [MANUAL_IMPORT]

# [/MANUAL_IMPORT]


'''
Created on Thu May 14 2020
@author: gr860
'''
class PickpartfromconveyorSM(Behavior):
	'''
	A simple pick and place pipe line
	'''


	def __init__(self):
		super(PickpartfromconveyorSM, self).__init__()
		self.name = 'Pick part from conveyor'

		# parameters of this behavior

		# references to used behaviors

		# Additional initialization code can be added inside the following tags
		# [MANUAL_INIT]
		
		# [/MANUAL_INIT]

		# Behavior comments:



	def create(self):
		joint_names = ['robot1_shoulder_pan_joint', 'robot1_shoulder_lift_joint', 'robot1_elbow_joint', 'robot1_wrist_1_joint', 'robot1_wrist_2_joint', 'robot1_wrist_3_joint']
		pi = 3.14159
		# x:10 y:439, x:22 y:364
		_state_machine = OperatableStateMachine(outcomes=['finished', 'failed'])
		_state_machine.userdata.speed = 100
		_state_machine.userdata.joint_values = []
		_state_machine.userdata.joint_names = []
		_state_machine.userdata.pose = []

		# Additional creation code can be added inside the following tags
		# [MANUAL_CREATE]
		
		# [/MANUAL_CREATE]


		with _state_machine:
			# x:172 y:67
			OperatableStateMachine.add('move_robot1_home',
										omtp_factory_flexbe_states__SrdfStateToMoveit(config_name='R1Home', move_group='robot1', action_topic='/move_group', robot_name=""),
										transitions={'reached': 'Start_conveyor', 'planning_failed': 'failed', 'control_failed': 'failed', 'param_error': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off, 'param_error': Autonomy.Off},
										remapping={'config_name': 'config_name', 'move_group': 'move_group', 'robot_name': 'robot_name', 'action_topic': 'action_topic', 'joint_values': 'joint_values', 'joint_names': 'joint_names'})

			# x:424 y:123
			OperatableStateMachine.add('Start_conveyor',
										SetConveyorPowerState(stop=False),
										transitions={'succeeded': 'Start_feeder', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'speed': 'speed'})

			# x:673 y:201
			OperatableStateMachine.add('Start_feeder',
										ControlFeederState(activation=True),
										transitions={'succeeded': 'move_robot1_pregrasp', 'failed': 'failed'},
										autonomy={'succeeded': Autonomy.Off, 'failed': Autonomy.Off})

			# x:1110 y:289
			OperatableStateMachine.add('Detect_object_on_conveyor',
										DetectPartCameraState(ref_frame='world', camera_topic='/omtp/logical_camera', camera_frame='logical_camera_frame'),
										transitions={'continue': 'wait_for_object', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'pose'})

			# x:910 y:241
			OperatableStateMachine.add('move_robot1_pregrasp',
										omtp_factory_flexbe_states__SrdfStateToMoveit(config_name='R1PreGrasp', move_group='robot1', action_topic='/move_group', robot_name=""),
										transitions={'reached': 'Detect_object_on_conveyor', 'planning_failed': 'failed', 'control_failed': 'failed', 'param_error': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off, 'param_error': Autonomy.Off},
										remapping={'config_name': 'config_name', 'move_group': 'move_group', 'robot_name': 'robot_name', 'action_topic': 'action_topic', 'joint_values': 'joint_values', 'joint_names': 'joint_names'})

			# x:1275 y:565
			OperatableStateMachine.add('Compute_grasp',
										ComputeGraspState(group='robot1', offset=0.16, joint_names=joint_names, tool_link='vacuum_gripper1_suction_cup', rotation=pi),
										transitions={'continue': 'move_robot1_to_computed_grasp', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'pose', 'joint_values': 'joint_values', 'joint_names': 'joint_names'})

			# x:1117 y:637
			OperatableStateMachine.add('move_robot1_to_computed_grasp',
										flexbe_manipulation_states__MoveitToJointsDynState(move_group='robot1', action_topic='/move_group'),
										transitions={'reached': 'activate_gripper1', 'planning_failed': 'failed', 'control_failed': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off},
										remapping={'joint_values': 'joint_values', 'joint_names': 'joint_names'})

			# x:922 y:739
			OperatableStateMachine.add('activate_gripper1',
										VacuumGripperControlState(enable=True, service_name='/gripper1/control'),
										transitions={'continue': 'move_robot1_to_pregrasp_again', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off})

			# x:435 y:771
			OperatableStateMachine.add('move_robot1_to_place',
										omtp_factory_flexbe_states__SrdfStateToMoveit(config_name='R1Place', move_group='robot1', action_topic='/move_group', robot_name=""),
										transitions={'reached': 'deactivate_gripper1', 'planning_failed': 'failed', 'control_failed': 'failed', 'param_error': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off, 'param_error': Autonomy.Off},
										remapping={'config_name': 'config_name', 'move_group': 'move_group', 'robot_name': 'robot_name', 'action_topic': 'action_topic', 'joint_values': 'joint_values', 'joint_names': 'joint_names'})

			# x:156 y:747
			OperatableStateMachine.add('deactivate_gripper1',
										VacuumGripperControlState(enable=False, service_name='/gripper1/control'),
										transitions={'continue': 'finished', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off})

			# x:1247 y:367
			OperatableStateMachine.add('wait_for_object',
										WaitState(wait_time=5),
										transitions={'done': 'Detec_object_on_conveyor_again'},
										autonomy={'done': Autonomy.Off})

			# x:1209 y:445
			OperatableStateMachine.add('Detec_object_on_conveyor_again',
										DetectPartCameraState(ref_frame='world', camera_topic='/omtp/logical_camera', camera_frame='logical_camera_frame'),
										transitions={'continue': 'Compute_grasp', 'failed': 'failed'},
										autonomy={'continue': Autonomy.Off, 'failed': Autonomy.Off},
										remapping={'pose': 'pose'})

			# x:668 y:779
			OperatableStateMachine.add('move_robot1_to_pregrasp_again',
										omtp_factory_flexbe_states__SrdfStateToMoveit(config_name='R1PreGrasp', move_group='robot1', action_topic='/move_group', robot_name=""),
										transitions={'reached': 'move_robot1_to_place', 'planning_failed': 'failed', 'control_failed': 'failed', 'param_error': 'failed'},
										autonomy={'reached': Autonomy.Off, 'planning_failed': Autonomy.Off, 'control_failed': Autonomy.Off, 'param_error': Autonomy.Off},
										remapping={'config_name': 'config_name', 'move_group': 'move_group', 'robot_name': 'robot_name', 'action_topic': 'action_topic', 'joint_values': 'joint_values', 'joint_names': 'joint_names'})


		return _state_machine


	# Private functions can be added inside the following tags
	# [MANUAL_FUNC]
	
	# [/MANUAL_FUNC]
